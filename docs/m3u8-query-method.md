### 纤纤视频网获取M3U8视频链接

**网站**

[纤纤影视](https://www.xian313.top/)

**网站栏目分类**

https://www.xian313.top/js/aaajs/hengfu.js

| 横幅栏目类型 | 链接                              |
| ------------ | --------------------------------- |
| 视频一       | https://www.xian313.top/L/26.html |
| 视频二       | https://www.xian313.top/L/22.html |
| 视频三       | https://www.xian313.top/L/23.html |
| 视频四       | https://www.xian313.top/L/32.html |
| 原创区       | https://www.xian313.top/L/30.html |
| 字幕一       | https://www.xian313.top/L/27.html |
| 字幕二       | https://www.xian313.top/L/28.html |
| 骑兵一       | https://www.xian313.top/L/17.html |
| 骑兵二       | https://www.xian313.top/L/33.html |
| 步兵区       | https://www.xian313.top/L/24.html |
| 主播区       | https://www.xian313.top/L/31.html |
| 欧美区       | https://www.xian313.top/L/29.html |
| H动漫        | https://www.xian313.top/L/25.html |

**资源列表**

视频资源:

```js
<ul class="primary-list min-video-list fn-clear">
    <li>
    	<a href="/V/190f540012d199aa.html" title="【3D】三个美女*******">
            <img src="https://pic.888xianxian.org/pic/uploadimg/2023-2/434723.jpg">
    <li>
    	<a href="">
```

列表页码:

```js
<div class="primary-page fn-clear">
	<div class="ui-pages">
        <a href="/L/23_444.html">页尾</a>
```

**视频页面**

视频大列表

```js
<div class="ui-box ui-box-big play-list-box" id="baiduhd-pl-list">
    <div class="ui-cnt play-list">
        <ul id="player_1">
            <li>
            	<a title="第1集" href="/player/5873c5661d13859b-0-0.html" target="_blank">第1集</a>
            <li>
            	<a title="第2集" href="/player/5873c5661d13859b-0-1.html" target="_blank">第1集</a>
<div class="ui-box ui-box-big play-list-box" id="baiduhd-pl-list">
    <div class="ui-cnt play-list">
        <ul id="player_2">
            <li>
            	<a title="第1集" href="/player/5873c5661d13859b-1-0.html" target="_blank">第1集</a>
            <li>
            	<a title="第2集" href="/player/5873c5661d13859b-1-1.html" target="_blank">第1集</a>
```

**视频链接**

```js
<div id="play-focus">
    <div class="layout" id="play-box">
		<div class="palyleft">
			<div class="playing">
				<script type="text/javascript" src="/playdata/79/443471.js?55365.5">
```

**js保存的M3U8链接**

```js
var VideoListJson = [
    ['slm3u8', [
      '\u7B2C1\u96C6$https://vip2.slbfsl.com/20230305/7pOj0lSj/index.m3u8$slm3u8'
    ]],
    ['som3u8', [
      '\u7B2C1\u96C6$https://play2.sewobofang.com/20230119/aZ1Pvcbf/index.m3u8$som3u8'
    ]]
  ],
  urlinfo = 'https://' + document.domain + '/player/5873c5661d13859b-<from>-<pos>.html';

/**
* urlinfo 中<from>与<post> 与 VideoListJson 的关系
* <from> 替代，指代VideoListJson这个Array对象的第几个元素（即索引）
* <pos> 替代数字，指VideoListJson的<from>索引下的Array元素的第二个元素的第<pos>个元素
* 例如: '/player/5873c5661d13859b-0-0.html' 代表 'slm3u8' 所在Array的第二个元素（是个Array)的第0个元素，即  '\u7B2C1\u96C6$https://vip2.slbfsl.com/20230305/7pOj0lSj/index.m3u8$slm3u8'
* /player/5873c5661d13859b-1-0.html 指代  '\u7B2C1\u96C6$https://play2.sewobofang.com/20230119/aZ1Pvcbf/index.m3u8$som3u8'
*/

var VideoListJson = [
    ['ffm3u8', [
      '\u7B2C01\u96C6$https://vip.ffzy-play5.com/20221021/224_fd359f32/index.m3u8$ffm3u8',
      '\u7B2C02\u96C6$https://vip.ffzy-play5.com/20221021/225_ef0a87cc/index.m3u8$ffm3u8',
      '\u7B2C03\u96C6$https://vip.ffzy-online5.com/20221025/409_2fc9471c/index.m3u8$ffm3u8',
      '\u7B2C04\u96C6$https://vip.ffzy-play5.com/20221028/709_2c693461/index.m3u8$ffm3u8',
      '\u7B2C05\u96C6$https://vip.ffzy-play5.com/20221104/1074_cd2daf67/index.m3u8$ffm3u8',
      '\u7B2C06\u96C6$https://vip.ffzy-play5.com/20221111/2156_f8216960/index.m3u8$ffm3u8',
      '\u7B2C07\u96C6$https://vip.ffzy-online5.com/20230226/8333_d9a45749/index.m3u8$ffm3u8',
      '\u7B2C08\u96C6$https://vip.ffzy-play5.com/20230304/8021_94bbcef9/index.m3u8$ffm3u8',
      '\u7B2C09\u96C6$https://vip.ffzy-play5.com/20230311/8391_e653be2f/index.m3u8$ffm3u8',
      '\u7B2C10\u96C6$https://vip.ffzy-play5.com/20230317/8763_991b54c5/index.m3u8$ffm3u8',
      '\u7B2C11\u96C6$https://vip.ffzy-play5.com/20230325/9147_8d18d159/index.m3u8$ffm3u8',
      '\u7B2C12\u96C6$https://vip.ffzy-play5.com/20230331/9579_22dde0a0/index.m3u8$ffm3u8'
    ]]
  ],
  urlinfo = 'https://' + document.domain + '/player/903abbd35c774360-<from>-<pos>.html'

/**
* /player/903abbd35c774360-0-0.html -> https://vip.ffzy-play5.com/20221021/224_fd359f32/index.m3u8
* /player/903abbd35c774360-0-1.html -> https://vip.ffzy-play5.com/20221021/225_ef0a87cc/index.m3u8
* /player/903abbd35c774360-0-1.html -> https://vip.ffzy-online5.com/20221025/409_2fc9471c/index.m3u8
* 可以确定视频播放页面和所播放链接之间的关系
**/
```

