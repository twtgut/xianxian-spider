import requests
from log import logger
import json
from bs4 import BeautifulSoup
import re
from js2py import EvalJs
class Api():
    def __init__(
        self,
        host: str = 'www.xian313.top',
        port: int = 443,
        timeout: int = 10
    ):
        self.host = host
        self.port = port
        self.timeout = timeout
        self.client = requests.Session()

    def __parseHtml(
            self,
            html_doc
    ):
        soup = BeautifulSoup(html_doc, 'html.parser')
        return soup
    
    def GetVideoTypeList(
        self,
        type_list_js: str = None
    ) -> dict:
        """ 获取视频类型列表
        type_list_js: 保存视频类型的js文件路径,可以根据js文件解析视频类型列表(可选)
        """
        video_type_dict = dict()
        if type_list_js:
            content = self.get(
                type_list_js
            )
            
            for link in self.__parseHtml(content).find(id="nav")[0].find_all('li'):
                video_type_dict[link.a.string.decode()] = link.a.get('href')

        # TODO 从配置文件中获取video_type_list

        return video_type_dict
    
    def GetVideoListPageNumByType(
            self,
            type_num: int
    ):
        """ 根据视频类型，获取视频资源页数
        type_num: 视频类型
        """

        content = self.get(
            f'L/{type_num}.html'
        )

        for link in self.__parseHtml(content).find(_class="primary-page fn-clear")[0].find(_class="ui-pages")[0].find_all('a'):
            if link.string.decode() == '页尾':
                ret = re.findall(r"\d+_(\d+).html",link.get("href"))
                if ret:
                    num = int(ret[0])
                    return num
                
        return 0

    def GetVideosListByTypeAndPageNum(
        self,
        type_num: int,
        page_num: int
    ):
        """ 根据视频类型和视频资源页数索引，获取视频资源列表
        type_num: 视频类型
        page_num: 视频资源页数索引
        """
        
        # 第一页需要特殊处理，数字从2开始
        if page_num <= 1:
            page_num_str = ""
        else:
            page_num_str = "_" + str(page_num)

        content = self.get(
            f'L/{type_num}{page_num_str}.html',
        )

        videos_list = list()
        for link in self.__parseHtml(content).find(_class="primary-list min-video-list fn-clear")[0].find('li'):
            ret = re.findall(r"/V/(.*).html", link.a.get('href'))
            if ret:
                v_index = ret[0]
            v_title = link.a.get('title')
            v_img = link.a.img.get('src')
            videos_list.append({
                'index': v_index,
                'title': v_title,
                'img': v_img 
            })
    
        return videos_list
    
    def GetVideoListByPlayerIndex(
            self,
            player_index: str
    ):
        """ 根据播放索引获取视频播放页面链接
        player_index: 视频资源页索引
        """
        
        content = self.get(
            f"V/{player_index}.html"
        )

        videoList = list()
        _p = dict()
        for link in self.__parseHtml(content).find(_class="ui-cnt play-list"):
            player = link.ul.get("id")
            _p["palyer"]= player
            _p["video"] = list()
            for _link in link.find('a'):
                v_title = _link.get('title')
                ret = re.findall(r"/player/(.*).html",_link.get('href'))
                if ret:
                    v_index = ret[0]
                _p["video"].append({
                    'title': v_title,
                    'index': v_index
                })
            
            videoList.append(_p)

        return videoList

    def GetVideoM3U8Links(
            self,
            playing_index: str
    ):
        """ 根据视频播放页索引获取M3U8链接
        playing_index: 视频播放页索引
        """
        
        content = self.get(
            f'player/{playing_index}.html'
        )

        link_js = None
        for link in self.__parseHtml(content).find(_class="playing"):
            link_js = link.script[0].get("src")

        if link_js:
            content = self.get(
                link_js
            )
            context = EvalJs()
            context.execute(content)
            m3u8_list = content.VideoListJson
            
            return m3u8_list

        return []
    
    #######################
    def baseRequest(
        self,
        method: str,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None
    ) -> dict:
        """基础请求方法"""

        try:
            resp = self.client.request(
                method,
                f'http://{self.host}:{self.port}/{path}',
                headers= headers,
                params= params,
                data = json.dumps(payload),
                timeout= self.timeout
            )
            resp.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if isinstance(e, requests.exceptions.Timeout):
                logger.warning(f'响应超时! => {e}')
            elif isinstance(e, requests.exceptions.HTTPError):
                logger.error(f'响应码出错 => {resp.status_code}')
            else:
                logger.error(f'请求出错')
            
            print(resp.text)
            return ""

        # 处理数据
        try:
            data = resp.text
        except Exception as e:
            logger.error('获取网页内容错误')
            return ""
        
        if data is None:
            logger.error('返回为 NULL')
            return ""

        return data

    def post(
        self,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None,
    ):
        return self.baseRequest(
            'POST',
            path,
            params= params, payload= payload, headers= headers
        )  

    def get(
        self,
        path: str,
        params: dict = None,
        headers: dict = None,
        payload: dict = None,
    ):
        return self.baseRequest(
            'GET',
            path,
            params= params, headers= headers, payload= payload
        )

import sys
if __name__ == '__main__':
    logger.info(f"{sys.argv[0]}: 接口")